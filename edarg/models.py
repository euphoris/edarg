from sqlalchemy import (Boolean, Column, ForeignKey, Integer, String, Text,
                        Table)
from sqlalchemy.orm import relationship
from webpat.models import Base, User


class Student(Base):
    __tablename__ = 'students'

    pk = Column(Integer, primary_key=True)
    name = Column(String(64))


class Test(Base):
    __tablename__ = 'tests'

    pk = Column(Integer, primary_key=True)
    title = Column(String(128))
    problems = relationship("Problem", backref='test')


class Problem(Base):
    __tablename__ = 'problems'

    pk = Column(Integer, primary_key=True)
    test_pk = Column(Integer, ForeignKey('tests.pk'))
    question = Column(Text)
    answers = relationship("Answer", backref="problem")
    subsets = relationship("Subset", backref="problem")


inclusion_table = Table('inclusion', Base.metadata,
                        Column('answer_pk', Integer, ForeignKey('answers.pk')),
                        Column('subset_pk', Integer, ForeignKey('subsets.pk')))


class Subset(Base):
    __tablename__ = 'subsets'
    pk = Column(Integer, primary_key=True)
    problem_pk = Column(Integer, ForeignKey('problems.pk'))
    description = Column(Text)
    answers = relationship("Answer",
                           secondary=inclusion_table,
                           backref="subsets")
    grades = relationship("Grade", backref="subset")


class Answer(Base):
    __tablename__ = 'answers'

    pk = Column(Integer, primary_key=True)
    student_pk = Column(Integer, ForeignKey('students.pk'))
    problem_pk = Column(Integer, ForeignKey('problems.pk'))
    answer = Column(Text)
    example_score = Column(Integer)

    TYPE_TEXT = 1
    TYPE_IMAGE = 2
    answer_type = Column(Integer, default=TYPE_TEXT)

    grades = relationship("Grade", backref="answer")


class Grade(Base):
    __tablename__ = 'grades'

    pk = Column(Integer, primary_key=True)
    answer_pk = Column(Integer, ForeignKey('answers.pk'))
    subset_pk = Column(Integer, ForeignKey('subsets.pk'))
    grader_pk = Column(Integer, ForeignKey('users.pk'))
    point = Column(Integer)
    time = Column(Integer)  # time taken for grading


class GraderInfo(Base):
    __tablename__ = 'graderinfo'
    grader_pk = Column(Integer, ForeignKey('users.pk'), primary_key=True)
    gender = Column(Integer)
    birth_year = Column(Integer)


class Order():
    first_first = 0
    last_first = 1
    random = 2


class Policy(Base):
    __tablename__ = 'policies'
    pk = Column(Integer, primary_key=True)
    num_ref = Column(Integer)  # the number of references
    # num_ref is not int, but str in pypy. I don't know why.
    more_ref = Column(Boolean, default=False)  # more references
    min_point = Column(Integer, default=1)
    max_point = Column(Integer, default=5)
    grading_order = Column(Integer, default=Order.random)
    ref_order = Column(Integer, default=Order.random)
    time_limit = Column(Integer, nullable=True)

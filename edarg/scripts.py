from argparse import ArgumentParser

from webpat.account import sign_up
from webpat.db import db
from edarg import EdargApp
from edarg.models import Problem, Student, Subset, Test


def run_server(args):
    app = EdargApp('sqlite:///edarg.db')
    app.run(host="0.0.0.0", debug=True)


def load_devel_fixture(args):
    app = EdargApp('sqlite:///edarg.db')
    with app.test_request_context():
        with app.test_client():
            sign_up(username='grader', password='grader')
            with db.begin():
                db.add_all([
                    Student(pk=1, name='student'),
                    Test(pk=1, title='test'),
                    Problem(pk=1, test_pk=1, question='abc'),
                    Problem(pk=2, test_pk=1, question='cde'),
                    Subset(problem_pk=1),
                    Subset(problem_pk=2),
                    ])


def run_script():
    parser = ArgumentParser(prog='edarg')
    subparsers = parser.add_subparsers(help='sub-command help')

    parser_server = subparsers.add_parser('server', help='run a test server')
    parser_server.set_defaults(func=run_server)

    help_load = 'load a development fixture'
    parser_load = subparsers.add_parser('load', help=help_load)
    parser_load.set_defaults(func=load_devel_fixture)

    args = parser.parse_args()
    args.func(args)

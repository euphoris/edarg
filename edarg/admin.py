from collections import defaultdict, Counter
import contextlib
import functools

import chardet
from flask import (Blueprint, Response, abort, redirect, request,
                   render_template, url_for)
from flask.ext.login import current_user, login_required
from webpat.db import db

from .models import Answer, Grade, User, Policy, Problem, Student, Test, Subset
from .compat import csv, csv_dict_reader, HTTP, StringIO


bp = Blueprint('admin', __name__)


def admin_only(f):
    @functools.wraps(f)
    def g(*args, **kwargs):
        if current_user.is_active() and current_user.is_admin:
            return f(*args, **kwargs)
        return redirect(url_for('member.view_login'))

    return g


@bp.route('/be_admin')
@login_required
def be_admin():
    num_admin = db.query(User).filter_by(is_admin=True).count()
    if num_admin > 0:
        abort(HTTP.NOT_FOUND)
    else:
        with db.begin():
            current_user.is_admin = True
            db.merge(current_user)
        return redirect(url_for('admin.home'))


@bp.route('/')
@admin_only
def home():
    tests = db.query(Test).all()
    problems = db.query(Problem).all()
    subsets = db.query(Subset).order_by(Subset.problem_pk)
    return render_template('admin/home.html',
                           tests=tests,
                           subsets=subsets,
                           problems=problems)


@bp.route('/upload', methods=['POST'])
@admin_only
def upload_data():
    f = request.files['file']

    encoding = chardet.detect(f.read())['encoding']
    f.seek(0)

    reader = csv_dict_reader(f, encoding=encoding)

    with db.begin():
        test = Test(title='from upload data')
        db.add(test)

    questions = set(reader.fieldnames) - set([u'name', u'example_score'])
    problem_dict = {}
    for question in questions:
        with db.begin():
            problem = Problem(test_pk=test.pk, question=question)
            db.add(problem)
        problem_dict[question] = problem.pk

    for row in reader:
        with db.begin():
            student = Student(name=row['name'].strip())
            db.add(student)

        for question in questions:
            problem_pk = problem_dict[question]
            with db.begin():
                answer = Answer(student_pk=student.pk,
                                problem_pk=problem_pk,
                                answer=row[question].strip())
                example_score = row.get('example_score')
                if example_score:
                    answer.example_score = example_score
                db.add(answer)

    return ''


def grader_student_table():
    """
    Return grader x student table.

    :returns: grade_dic, student_names
    :rtype: (dict, list)
    """
    grades = db.query(Answer, Grade, User, Problem, Student) \
        .filter(Grade.answer_pk == Answer.pk) \
        .filter(Answer.student_pk == Student.pk) \
        .filter(Answer.problem_pk == Problem.pk) \
        .filter(Grade.grader_pk == User.pk) \
        .order_by(Grade.pk)

    grades_dic = defaultdict(Counter)
    student_names = set()
    for answer, grade, user, problem, student in grades:
        grades_dic[user.username][student.name] = grade.point
        grades_dic[user.username]['grader'] = user.username
        student_names.add(student.name)
    student_names = sorted(student_names)

    return grades_dic, student_names


@bp.route('/grades')
@admin_only
def view_grades():
    grades_dic, student_names = grader_student_table()
    return render_template('admin/view_grades.html',
                           grades_dic=grades_dic,
                           student_names=student_names)


@bp.route('/grades/csv')
@admin_only
def download_grades():
    grades_dic, student_names = grader_student_table()

    fieldnames = ['grader']
    fieldnames.extend(student_names)

    with contextlib.closing(StringIO()) as f:
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()
        for grader in grades_dic:
            writer.writerow(grades_dic[grader])
        value = f.getvalue()
    return Response(value,
                    mimetype='text/csv',
                    headers={"Content-Disposition":
                             "attachment;filename=test.csv"})


@bp.route('/policy')
@admin_only
def get_policy():
    """
    :return: policy setting view
    """
    policy = db.query(Policy).one()
    return render_template('admin/policy.html', policy=policy)


def get_int(name, default=None):
    """
    Get an int value from request.form.

    :param name: parameter name
    :param default: default value
    :rtype: int
    """
    try:
        value = int(request.form.get(name, default))
    except ValueError:
        value = default
    except TypeError:
        value = default
    return value


@bp.route('/subset/new/<int:problem_pk>')
@admin_only
def new_subset(problem_pk):
    subset = Subset(problem_pk=problem_pk)

    problem = db.query(Problem).get(problem_pk)
    answers = db.query(Answer, Student) \
        .filter(Answer.student_pk == Student.pk) \
        .filter(Answer.problem_pk == problem_pk) \
        .all()

    return render_template('admin/subset.html',
                           subset=subset,
                           problem=problem,
                           answers=answers,
                           inclusion=[])


@bp.route('/subset/<int:subset_pk>')
@admin_only
def edit_subset(subset_pk):
    subset = db.query(Subset).get(subset_pk)
    answers = db.query(Answer, Student) \
        .filter(Answer.student_pk == Student.pk) \
        .filter(Answer.problem_pk == subset.problem_pk) \
        .all()
    inclusion = [a.pk for a in subset.answers]

    return render_template('admin/subset.html',
                           subset=subset,
                           problem=subset.problem,
                           answers=answers,
                           inclusion=inclusion)


@bp.route('/subset/save', methods=['POST'])
@admin_only
def save_subset():
    """
    Create a new policy.

    :return:
    """
    subset_pk = request.form.get('pk')
    if subset_pk:
        subset = db.query(Subset).get(subset_pk)
        inclusion = set(a.pk for a in subset.answers)
    else:
        subset = Subset(problem_pk=request.form.get('problem'))
        inclusion = set()

    answers = set(int(a) for a in request.form.getlist('answers'))

    for added in (answers - inclusion):
        answer = db.query(Answer).get(added)
        subset.answers.append(answer)

    for removed in (inclusion - answers):
        answer = db.query(Answer).get(removed)
        subset.answers.remove(answer)

    with db.begin():
        db.merge(subset)

    return redirect(url_for('admin.home'))


@bp.route('/policy', methods=['POST'])
@admin_only
def set_policy():
    """
    :return: policy setting result
    """

    policy = db.query(Policy).one()

    with db.begin():
        policy.num_ref = request.form.get('num_ref')
        policy.more_ref = request.form.get('more_ref', 0) == '1'

        min_point = get_int('min_point', policy.min_point)
        max_point = get_int('max_point', policy.max_point)
        if min_point < max_point:
            policy.min_point = min_point
            policy.max_point = max_point

        policy.grading_order = get_int('grading_order', 2)
        policy.ref_order = get_int('ref_order', 2)
        policy.time_limit = get_int('time_limit')

        db.merge(policy)

    return render_template('admin/policy.html', policy=policy)


@bp.route('/test/new', methods=['GET', 'POST'])
@admin_only
def new_test():
    if request.method == 'GET':
        return render_template('admin/new_test.html')

    with db.begin():
        title = request.form.get('title')
        test = Test(title=title)
        db.add(test)
    return redirect(url_for('admin.home'))


@bp.route('/test/<int:test_pk>', methods=['GET', 'POST'])
@admin_only
def edit_test(test_pk):
    test = db.query(Test).get(test_pk)
    if not test:
        return abort(404)
    if request.method == 'POST':
        problem = Problem(question=request.form.get('question'))
        with db.begin():
            test.problems.append(problem)
        subset = Subset(problem_pk=problem.pk)
        with db.begin():
            problem.subsets.append(subset)
    return render_template('admin/edit_test.html',
                           test=test, problems=test.problems)

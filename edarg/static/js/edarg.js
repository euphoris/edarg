(function($){

  $.fn.shuffle = function() {

    var allElems = this.get(),
      getRandom = function(max) {
        return Math.floor(Math.random() * max);
      },
      shuffled = $.map(allElems, function(){
        var random = getRandom(allElems.length),
          randEl = $(allElems[random]).clone(true)[0];
        allElems.splice(random, 1);
        return randEl;
      });

    this.each(function(i){
      $(this).replaceWith($(shuffled[i]));
    });

    return $(shuffled);

  };

})(jQuery);

$(function(){
  var timestamp;

  var setItem = function(){
    var item = $('ul#answers li:first');
    if( item.length > 0 ){
      timestamp = new Date();
      $('div.answer').html(item.html());
      $('input[name=answer_pk]').attr('value', item.attr('data-pk'));
      $('input[type=radio]:checked').removeAttr('checked');
      return false;
    }
    else {
      $('form#aggregated').submit();
    }
  }
  setItem();

  var getPolicy = function(name){
    return parseInt($('#policy input[name="'+name+'"]').val());

  }

  var visible_ref = 0,
      num_ref = getPolicy('num_ref'), // number of reference
      ref_order = getPolicy("ref_order"), // reference order
      time_limit = getPolicy("time_limit");

  // redirect to problem list on time out
  var stopGrading = function(){
    alert('stop');
    window.location = '/grader';
  }

  if ( !isNaN(time_limit) ){
    setTimeout(stopGrading, time_limit*1000);
  }

  $('button.submit').click(function(){
    var checked = $('input[type=radio]:checked');
    if( checked.length > 0 ){
      var form = $(this).parents('form'),
          url = form.attr('action'),
          time = (new Date()) - timestamp,
          data = form.serialize()+'&time='+time;
      $.post(url, data);

      var aggregated = $('#aggregated > textarea');
      aggregated.val(aggregated.val() + data + '\n');

      // move the graded answer to the reference list
      var graded_answer = $('ul#answers li:first');
      graded_answer.attr('data-point', $(checked[0]).val());

      if (ref_order == 0){ // the first comes first
        graded_answer.appendTo('#group');
      }
      else { // the last comes first or random
        graded_answer.prependTo('#group');
      }

      $('#group li').hide();
      if (ref_order == 2){ // shuffle group if random
        $('#group li').shuffle();
      }

      $('button#more_ref').hide();
      setItem();
    }
    else {
      $('div#no-point-error').slideDown();
    }
    return false;
  });

  var showRef = function(point){
    var refs = $('#group li[data-point='+point+']');
    if( isNaN(visible_ref) ){
      refs.show();
    }
    else {
      refs.slice(0, visible_ref).show();
      if( visible_ref > 0 ) $('button#more_ref').show();
    }
  };


  $('input[type=radio]').change(function(){
    $('div#no-point-error').slideUp();
    $('#group li').hide();
    visible_ref = num_ref;
    showRef($(this).val());
  });


  $('button#more_ref').click(function(){ // more references button
    visible_ref += num_ref;
    var point = $('input[type=radio]:checked').val();
    showRef(point);
    return false;
  });

});
// vim: set ts=2 sw=2 sts=2:

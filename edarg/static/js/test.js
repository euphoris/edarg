function getCanvas(){
  var canvas = $('.ui-active > canvas');
  canvas.context = canvas[0].getContext('2d');
  return canvas
}

function setCanvas(){
  var status = false,
      brushType = '',
      lastX = -1,
      lastY = -1,
      canvas = getCanvas(),
      context = canvas.context;

  function brushStart(e){
    var offset = canvas.offset(),
        top = offset.top,
        left = offset.left;
    status = true;
    lastX = e.pageX - left;
    lastY = e.pageY - top;
    // pen or eraser
    brushType = $('input[name=brush]:checked').attr('value');
    if( !brushType ) brushType = 'pen';
    if( brushType == 'pen' ){
      context.strokeStyle = '#000';
      context.lineWidth = 3;
    } else {
      context.strokeStyle = '#FFF';
      if (brushType == 'eraser_small') context.lineWidth = 10;
      else if (brushType == 'eraser_med') context.lineWidth = 30;
      else context.lineWidth = 50;
    }
  }

  function brushMove(e){
    if(status){
      var offset = canvas.offset(),
          top = offset.top,
          left = offset.left,
          right = left + canvas.width(),
          bottom = top + canvas.height();

      if( left < e.pageX && e.pageX < right &&
          top < e.pageY && e.pageY < bottom ){
        var context = canvas[0].getContext('2d');

        newX = e.pageX - left;
        newY = e.pageY - top;

        context.beginPath();
        context.moveTo(lastX, lastY);
        context.lineTo(newX, newY);
        context.stroke();

        lastX = newX;
        lastY = newY;
      }
    }
  }

  function brushEnd(e){
    status = false;
  }

  context.canvas.width = $('.problem').width();
  context.canvas.height = $('.problem').width();

  canvas.bind('mousedown', brushStart);
  canvas.bind('mousemove', brushMove);
  canvas.bind('mouseup', brushEnd);

  $('canvas')[0].addEventListener('touchstart', function(e){
    e.preventDefault();
    if (e.targetTouches) e = e.targetTouches[0];
    brushStart(e);
  },false);

  $('canvas')[0].addEventListener('touchmove', function(e){
    e.preventDefault();
    if (e.targetTouches) e = e.targetTouches[0];
    brushMove(e);
  },false);

  $('canvas')[0].addEventListener('touchend', function(e){
    e.preventDefault();
    if (e.targetTouches) e = e.targetTouches[0];
    brushEnd(e);
  },false);


  return context;
}


function loadUI(){
  var skel, context, img;
  // show ui
  skel = $('.ui-skeleton').clone();
  $('.save').click();
  skel.appendTo($(this).parent());
  skel.removeClass('ui-skeleton');
  skel.addClass('ui-active');

  // load the answer image
  context = setCanvas();
  img = $(this).parents('li').children('img')[0];
  if( img ){
    context.drawImage(img,0,0);
    $(img).remove();
  }

  $(this).remove();
  return false;
}


function uploadAnswers(){
  var response = [],
      url = $('form').attr('action');
  $('.save').click();

  $('#problems').slideUp();
  $(this).hide();
  $('.sending-alert').show();

  $('#problems li').each(function(){
    response.push({
      problem: $(this).attr('data-problem'),
      dataurl: $(this).find('.dataurl').text()
    });
  });

  $.post(url, {response: JSON.stringify(response) }, function(){
    $('.sending-alert').hide();
    $('.finish-alert').show();
  })
  return false;
}


$(function(){
  $(document).on('click', '.edit', loadUI);

  $(document).on('click', '.save', function(){
    var canvas, dataURL, li, btn_edit;

    canvas = $('.ui-active > canvas')[0];
    if( canvas ){
      dataURL = canvas.toDataURL();
      li = $(this).parents('li')
      li.append('<img src="'+dataURL+'">');
      li.children('textarea').text(dataURL)

      // edit button
      li.append('<p>');
      btn_edit = $('.edit:hidden').clone()
      btn_edit.appendTo(li);
      btn_edit.show();

      $(this).parents('.ui-active').remove();
    }
    return false;
  });

  $(document).on('click', '.clear', function(){
    var canvas = getCanvas();
    canvas.context.clearRect(0, 0, canvas.width(), canvas.height())
  });

  $('.upload').click(uploadAnswers);
  $('.btn').button();
});
// vim: set filetype=javascript ts=2 sw=2 sts=2:

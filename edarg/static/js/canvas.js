$(function(){
  var status = false,
      brushType = '',
      lastX = -1,
      lastY = -1;

  function brushStart(e){
    var canvas = $('canvas'),
        context = canvas[0].getContext('2d'),
        offset = canvas.offset(),
        top = offset.top,
        left = offset.left;
    status = true;
    lastX = e.pageX - left;
    lastY = e.pageY - top;
    // pen or eraser
    brushType = $('input[name=brush]:checked').attr('value');
    if( brushType == 'pen' ){
      context.strokeStyle = '#000';
      context.lineWidth = 3;
    } else {
      context.strokeStyle = '#FFF';
      if (brushType == 'eraser_small') context.lineWidth = 10;
      else if (brushType == 'eraser_med') context.lineWidth = 30;
      else context.lineWidth = 50;
    }
  }

  function brushMove(e){
    if(status){
      var canvas = $('canvas'),
          offset = canvas.offset(),
          top = offset.top,
          left = offset.left,
          right = left + canvas.width(),
          bottom = top + canvas.height();

      if( left < e.pageX && e.pageX < right &&
          top < e.pageY && e.pageY < bottom ){
        var context = canvas[0].getContext('2d');

        newX = e.pageX - left;
        newY = e.pageY - top;

        context.beginPath();
        context.moveTo(lastX, lastY);
        context.lineTo(newX, newY);
        context.stroke();

        lastX = newX;
        lastY = newY;
      }
    }
  }

  function brushEnd(e){
    status = false;
    $('textarea').text('end');
  }

  var canvas = $('canvas')[0],
      context = canvas.getContext('2d');

  context.canvas.width = $('.problem').width();
  context.canvas.height = $('.problem').width();

  $('canvas').bind('mousedown', brushStart);
  $('canvas').bind('mousemove', brushMove);
  $('canvas').bind('mouseup', brushEnd);

  $('canvas')[0].addEventListener('touchstart', function(e){
    e.preventDefault();
    if (e.targetTouches) e = e.targetTouches[0];
    brushStart(e);
  },false);

  $('canvas')[0].addEventListener('touchmove', function(e){
    e.preventDefault();
    if (e.targetTouches) e = e.targetTouches[0];
    brushMove(e);
  },false);

  $('canvas')[0].addEventListener('touchend', function(e){
    e.preventDefault();
    if (e.targetTouches) e = e.targetTouches[0];
    brushEnd(e);
  },false);

  $('.save').click(function(){
    var dataURL = $('canvas')[0].toDataURL(),
        url = $('form').attr('action');
    // $.post(url, { response: dataURL });
    $('textarea').text(dataURL);
    $('form').submit();
    return false;
  });

  $('.clear').click(function(){
    context.clearRect(0, 0, canvas.width, canvas.height)
  });

  $('.btn').button();
});
// vim: set ts=2 sw=2 sts=2:

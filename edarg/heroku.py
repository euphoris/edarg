import os
from edarg import EdargApp

uri = os.environ['DATABASE_URL']
app = EdargApp(uri)
app.config['DEBUG'] = True

import os

from flask import Blueprint, redirect, url_for
from flask.ext.login import current_user
from webpat.app import App
from sqlalchemy.orm import create_session

import edarg.admin
import edarg.grader
import edarg.member
import edarg.student
from edarg.models import Base, Policy


bp = Blueprint('main', __name__)


@bp.route('/')
@edarg.member.login_required
def home():
    if current_user.is_admin:
        return redirect(url_for('admin.home'))
    else:
        return redirect(url_for('grader.home'))


class EdargApp(App):
    def __init__(self, uri):
        super(EdargApp, self).__init__(__name__, uri=uri, base=Base)

        # default policy
        engine = self.config['DATABASE_ENGINE']
        sess = create_session(bind=engine)
        if sess.query(Policy).count() == 0:
            with sess.begin():
                sess.add(Policy())

        self.secret_key = os.urandom(24)

        self.register_blueprint(edarg.admin.bp, url_prefix='/admin')
        self.register_blueprint(edarg.grader.bp, url_prefix='/grader')
        self.register_blueprint(edarg.student.bp, url_prefix='/student')
        self.register_blueprint(edarg.member.bp, url_prefix='/member')
        self.register_blueprint(bp, url_prefix='/')

        self.login_manager.login_view = 'member.view_login'

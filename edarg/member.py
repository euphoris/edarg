from flask import Blueprint, redirect, request, render_template, url_for
from flask.ext.login import login_required, logout_user, current_user
from sqlalchemy.orm.exc import NoResultFound
from webpat.account import AccountException, MissingInfo, sign_in, sign_up
from webpat.db import db

from .models import GraderInfo

bp = Blueprint('member', __name__)


@bp.route('/signup')
def view_signup():
    if current_user.is_active():
        return redirect(url_for('grader.home'))
    return render_template('member/signup.html')


@bp.route('/signup', methods=['POST'])
def signup():
    username = request.form.get('username')
    password = request.form.get('password')
    gender = request.form.get('gender')
    birth_year = request.form.get('birth_year')

    try:
        sign_up(username=username, password=password)

    except AccountException:
        return render_template('member/signup.html', error="user exists")

    except MissingInfo:
        return render_template('member/signup.html',
                               username=username,
                               password=password,
                               error='username or password missing')

    sign_in(username=username, password=password)
    with db.begin():
        db.add(GraderInfo(grader_pk=current_user.get_id(),
                          gender=gender, birth_year=birth_year))
    return redirect(url_for('grader.home'))



@bp.route('/login')
def view_login():
    if current_user.is_active():
        return redirect(url_for('grader.home'))
    return render_template('member/login.html')


@bp.route('/login', methods=['POST'])
def login():
    username = request.form.get('username')
    password = request.form.get('password')

    try:
        sign_in(username=username, password=password)

    except NoResultFound:
        return render_template('member/login.html',
                               error='no user')
    except AccountException:
        return render_template('member/login.html',
                               username=username,
                               error='password incorrect')
    except MissingInfo:
        return render_template('member/login.html',
                               username=username,
                               password=password,
                               error='username or password missing')

    return redirect(url_for('grader.home'))


@bp.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('member.view_login'))

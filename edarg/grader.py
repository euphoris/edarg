from flask import Blueprint, abort, redirect, request, render_template, url_for
from flask.ext.login import current_user, login_required
from sqlalchemy.sql.expression import func
from webpat.db import db

from .compat import HTTP
from .models import Answer, Grade, Order, Policy, Subset


bp = Blueprint('grader', __name__)


@bp.route('/')
@login_required
def home():
    subsets = db.query(Subset)
    return render_template('grader/home.html', subsets=subsets)


@bp.route('/<int:subset_pk>')
@login_required
def list_answers(subset_pk):
    """
    Render the grading view
    """
    subset = db.query(Subset).get(subset_pk)
    if not subset:
        abort(HTTP.NOT_FOUND)

    policy = db.query(Policy).one()

    # grading order
    if policy.grading_order == Order.first_first:
        order = Answer.pk.asc()
    elif policy.grading_order == Order.last_first:
        order = Answer.pk.desc()
    else:
        order = func.random()

    # answers in the subset
    answers = db.query(Answer) \
        .join(Answer.subsets) \
        .filter(Subset.pk == subset_pk) \
        .order_by(order)

    # ref order
    if policy.ref_order == Order.first_first:
        order = Grade.pk.asc()
    elif policy.ref_order == Order.last_first:
        order = Grade.pk.desc()
    else:
        order = func.random()

    # answers already graded
    def get_grades():
        return db.query(Grade, Answer) \
                 .filter(Grade.answer_pk == Answer.pk,
                         Grade.grader_pk == current_user.pk,
                         Grade.subset_pk == subset_pk,
                         Answer.problem_pk == subset.problem_pk) \
                 .order_by(order)
    grades = get_grades()

    if grades.count() == 0:
        with db.begin():
            for answer in answers:
                if answer.example_score:
                    db.add(Grade(answer_pk=answer.pk,
                                 subset_pk=subset.pk,
                                 grader_pk=current_user.pk,
                                 point=answer.example_score,
                                 time=0))
        grades = get_grades()

    filtered_answers = []
    grade_set = set(a.pk for _, a in grades)
    for answer in answers:
        if answer.pk not in grade_set:
            filtered_answers.append(answer)
    answers = filtered_answers

    if not answers:
        return redirect(url_for('grader.home'))

    return render_template('grader/list_answers.html',
                           answers=answers,
                           subset=subset,
                           problem=subset.problem,
                           grades=grades,
                           policy=policy)


@bp.route('/grade', methods=['POST'])
@login_required
def grade():
    try:
        answer_pk = int(request.form.get('answer_pk'))
        subset_pk = int(request.form.get('subset_pk'))
        point = int(request.form.get('point'))
        time = int(request.form.get('time'))
    except:
        return 'parameter required'

    if not db.query(Answer).get(answer_pk):
        return 'no answer'

    with db.begin():
        db.add(Grade(answer_pk=answer_pk,
                     subset_pk=subset_pk,
                     grader_pk=current_user.pk,
                     point=point,
                     time=time))

    return 'ok'

@bp.route('/grade/all', methods=['POST'])
@login_required
def grade_all():
    data = request.form.get('data','').strip()
    if data:
        pass
    return redirect(url_for('grader.home'))


@bp.route('/grade/clear')
@login_required
def clear():
    db.query(Grade).filter_by(grader_pk=current_user.pk).delete()
    return redirect(url_for('grader.home'))


@bp.route('/group/<int:problem_pk>')
@login_required
def get_group(problem_pk):
    point = int(request.args['point'])
    grades = db.query(Grade, Answer)\
               .filter(Grade.grader_pk == current_user.pk,
                       Answer.problem_pk == problem_pk,
                       Grade.answer_pk == Answer.pk,
                       Grade.point == point)\
                .order_by(Grade.pk.desc())

    return render_template('grader/get_group.html', grades=grades)

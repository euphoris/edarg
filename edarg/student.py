# encoding: utf8
import json
from flask import Blueprint, abort, render_template, redirect, request, url_for
from webpat.db import db

from .models import Answer, Problem, Student, Test

bp = Blueprint('student', __name__)


@bp.route('/test/<int:test_pk>')
def take_test(test_pk):
    test = db.query(Test).get(test_pk)
    if not test:
        return abort(404)
    return render_template('student/test.html',
                           test=test, problems=test.problems)


@bp.route('/test/<int:test_pk>', methods=['POST'])
def response_test(test_pk):
    test = db.query(Test).get(test_pk)
    if not test:
        return abort(404)
    response = json.loads(request.form.get('response', '[]'))
    with db.begin():
        for res in response:
            problem_pk = res.get('problem')
            problem = db.query(Problem).get(problem_pk)
            if problem:
                answer = Answer(student_pk=1,
                                problem_pk=problem_pk,
                                answer_type=Answer.TYPE_IMAGE,
                                answer=res.get('dataurl'))
                subset = problem.subsets[0]
                subset.answers.append(answer)
    return 'OK'


@bp.route('/test/<int:test_pk>/<int:problem_pk>')
def solve_problem(test_pk, problem_pk):
    test = db.query(Test).get(test_pk)
    problem = db.query(Problem).get(problem_pk)
    if not test or not problem:
        return abort(404)
    return render_template('student/problem.html', test=test, problem=problem)


@bp.route('/problem/<int:test_pk>/<int:problem_pk>', methods=['POST'])
def receive_response(test_pk, problem_pk):
    test = db.query(Test).get(test_pk)
    problem = db.query(Problem).get(problem_pk)
    if not test or not problem:
        return abort(404)
    with db.begin():
        answer = Answer(student_pk=1,
                        problem_pk=problem_pk,
                        answer_type=Answer.TYPE_IMAGE,
                        answer=request.form['response'])
        subset = problem.subsets[0]
        subset.answers.append(answer)
    return redirect(url_for('student.take_test', test_pk=test_pk))


@bp.route('/show')
def show_response():
    answers = db.query(Answer).all()
    return render_template('student/show.html', answers=answers)


@bp.route('/new')
def new_student():
    with db.begin():
        db.add(Student(pk=1, name='test'))
    return redirect(url_for('student.take_test', test_pk=1))

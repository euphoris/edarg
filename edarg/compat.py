import sys


PY3 = sys.version_info >= (3,)


if PY3:
    import codecs
    import csv
    import http.client as HTTP
    from io import BytesIO as StringIO
    def csv_dict_reader(f, encoding):
        csv_reader = csv.DictReader(codecs.getreader(encoding)(f))
        return csv_reader
else:
    import unicodecsv as csv
    import httplib as HTTP
    from cStringIO import StringIO
    csv_dict_reader = csv.DictReader

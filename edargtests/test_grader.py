import re

from flask import url_for
from webpat.db import db

from edarg.compat import HTTP
from edarg.models import Grade, Order, Policy

from .fixtures import answer, client, example_score, user
from .utils import login


def test_problem_list(client, user, answer):
    login(client, user)
    res = client.get(url_for('grader.home'))
    assert res.status_code == HTTP.OK
    url = url_for('grader.list_answers', subset_pk=2)
    assert url in res.data.decode('utf8')


def test_answer_list_without_login(client, user):
    res = client.get(url_for('grader.list_answers', subset_pk=2))
    assert res.status_code == HTTP.FOUND


def test_answer_list_0(client, user):
    login(client, user)
    res = client.get(url_for('grader.list_answers', subset_pk=2))
    assert res.status_code == HTTP.NOT_FOUND


def test_answer_list_1(client, user, answer):
    """
    Show answers graded in ul#answers, answers not graded in ul#group
    """
    login(client, user)
    res = client.get(url_for('grader.list_answers', subset_pk=2))
    assert res.status_code == HTTP.OK
    data = res.data.decode('utf8')
    assert re.search(r'data-pk="1">\s+not_graded', data)
    assert re.search(r'data-point="3">\s+already_graded', data)


def test_answer_list_when_grading_completed(client, user, answer):
    login(client, user)
    with db.begin():
        db.add_all([
            Grade(answer_pk=1, subset_pk=2, grader_pk=1, point=1, time=0),
            Grade(answer_pk=3, subset_pk=2, grader_pk=1, point=1, time=0),
            ])
    res = client.get(url_for('grader.list_answers', subset_pk=2))
    assert res.status_code == HTTP.FOUND


def test_answer_list_policy(client, user, answer):
    """
    show policies in grading view
    """
    login(client, user)
    db.query(Policy).update(dict(num_ref=0))

    res = client.get(url_for('grader.list_answers', subset_pk=2))
    assert 'num_ref" value="0' in res.data.decode('utf8')


def test_answer_list_order(client, user, answer):
    login(client, user)
    db.query(Policy).update(dict(grading_order=Order.first_first))
    res = client.get(url_for('grader.list_answers', subset_pk=2))
    data = res.data.decode('utf8')
    first = data.find('not_graded')
    last = data.find('last_answer')
    assert first < last


def test_answer_list_reverse_order(client, user, answer):
    login(client, user)
    db.query(Policy).update(dict(grading_order=Order.last_first))
    res = client.get(url_for('grader.list_answers', subset_pk=2))
    data = res.data.decode('utf8')
    first = data.find('not_graded')
    last = data.find('last_answer')
    assert last < first


def test_ref_list_order(client, user, answer):
    login(client, user)
    db.query(Policy).update(dict(ref_order=Order.first_first))
    res = client.get(url_for('grader.list_answers', subset_pk=2))
    data = res.data.decode('utf8')
    first = data.find('already_graded')
    last = data.find('last_graded')
    assert first < last


def test_ref_list_reverse_order(client, user, answer):
    login(client, user)
    db.query(Policy).update(dict(ref_order=Order.last_first))
    res = client.get(url_for('grader.list_answers', subset_pk=2))
    data = res.data.decode('utf8')
    first = data.find('already_graded')
    last = data.find('last_graded')
    assert last < first


def test_example_score(client, user, example_score):
    login(client, user)
    res = client.get(url_for('grader.list_answers', subset_pk=3))
    assert re.search('data-point="99">\s+not_graded', res.data.decode('utf8'))


def test_grade_without_login(client, user):
    res = client.post(url_for('grader.grade'))
    assert res.status_code == HTTP.FOUND


def test_grade_without_parameter(client, user):
    login(client, user)
    res = client.post(url_for('grader.grade'))
    assert res.status_code == HTTP.OK
    assert u'parameter required' == res.data.decode("utf8")


def test_grade(client, user, answer):
    login(client, user)

    before = db.query(Grade).count()
    res = client.post(url_for('grader.grade'),
                      data={'answer_pk': 1,
                            'subset_pk': 1,
                            'point': 3,
                            'time': 123})
    after = db.query(Grade).count()

    assert res.status_code == HTTP.OK
    assert u'ok' == res.data.decode('utf8')

    assert before + 1 == after


def test_grade_no_answer(client, user):
    login(client, user)
    res = client.post(url_for('grader.grade'),
                      data={'answer_pk': 1,
                            'subset_pk': 1,
                            'point': 3,
                            'time': 123})
    assert u'no answer' == res.data.decode('utf8')


def test_get_group(client, user, answer):
    login(client, user)
    res = client.get(url_for('grader.get_group', problem_pk=1, point=3))
    assert res.status_code == HTTP.OK
    assert 'already_graded' in str(res.data)

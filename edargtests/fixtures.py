# encoding: utf-8
from tempfile import NamedTemporaryFile

import pytest
from webpat.db import db
from werkzeug.security import generate_password_hash

from edarg import EdargApp
from edarg.models import User, Student, Test, Problem, Answer, Grade, Subset


@pytest.fixture
def client(request):
    f = NamedTemporaryFile()
    
    uri = 'sqlite:///' + f.name
    app = EdargApp(uri)

    app.config['TESTING'] = True

    ctx = app.test_request_context()
    ctx.push()

    def fin():
        ctx.pop()
        f.close()
    request.addfinalizer(fin)

    return app.test_client()


@pytest.fixture
def user(request):
    with db.begin():
        pw_hash = generate_password_hash('1234')
        db.add_all([
            User(pk=1, username='test', password=pw_hash),
            User(pk=3, username='another', password=pw_hash),
        ])
    return 'test', '1234'


@pytest.fixture
def admin(request):
    with db.begin():
        pw_hash = generate_password_hash('1234')
        user = User(pk=2, username='admin', password=pw_hash, is_admin=True)
        db.merge(user)
    return 'admin', '1234'


@pytest.fixture
def answer(request, user):
    username, _ = user

    user = db.query(User).filter_by(username=username).one()

    with db.begin():
        db.add_all([
            Student(pk=1, name=u'학생'),
            Student(pk=2, name=u'학생'),

            Test(pk=1, title='test'),

            Problem(pk=1, test_pk=1, question='abc'),

            Subset(pk=1, problem_pk=1),
            ])

        subset = Subset(pk=2, problem_pk=1)
        subset = db.merge(subset)

        answers = [
            Answer(pk=1, student_pk=1, problem_pk=1, answer='not_graded'),
            Answer(pk=3, student_pk=1, problem_pk=1, answer='last_answer'),
            ]

        subset.answers.extend(answers)

        db.add_all([
            Answer(pk=2, student_pk=1, problem_pk=1, answer='already_graded'),
            Answer(pk=4, student_pk=1, problem_pk=1, answer='last_graded'),
            Grade(pk=1, answer_pk=2, subset_pk=subset.pk, grader_pk=user.pk,
                  point=3),
            Grade(pk=2, answer_pk=4, subset_pk=subset.pk, grader_pk=user.pk,
                  point=3),
            Grade(pk=3, answer_pk=3, subset_pk=subset.pk, grader_pk=3, point=3),
            Grade(pk=4, answer_pk=4, subset_pk=subset.pk, grader_pk=3, point=3),
            ])

        subset3 = db.merge(Subset(pk=3, problem_pk=1))
        subset3.answers.extend(answers)
    return ''


@pytest.fixture
def example_score(answer):
    answer = db.query(Answer).get(1)
    with db.begin():
        answer.example_score = 99
    return ''


@pytest.fixture
def test_csv(request):
    return u'''name,problem1,problem2,example_score
               김철수,블라블라,아하아하,99
               박영희,옷홋홋홋,따라라라'''

from flask import url_for
from webpat.db import db

from edarg.compat import HTTP
from edarg.models import Answer, Student

from .fixtures import answer, client, user


def test_taking_test(client, answer):
    res = client.get(url_for('student.take_test', test_pk=1))
    assert res.status_code == HTTP.OK


def test_solve_problem(client, answer):
    res = client.get(url_for('student.solve_problem', test_pk=1, problem_pk=1))
    assert res.status_code == HTTP.OK


def test_receive_answer(client, answer):
    answer_string = '1234'
    url = url_for('student.receive_response', test_pk=1, problem_pk=1)
    res = client.post(url, data={'response': answer_string})
    assert res.status_code == HTTP.FOUND

    answer = db.query(Answer).all()[-1]
    assert answer.answer_type == Answer.TYPE_IMAGE
    assert answer.answer == answer_string


def test_new_student(client):
    assert db.query(Student).count() == 0
    client.get(url_for('student.new_student'))
    assert db.query(Student).get(1)

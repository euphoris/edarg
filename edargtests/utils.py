from flask import url_for


def login(client, user):
    username, password = user
    client.post(url_for('member.login'),
                data={'username': username,
                      'password': password})

import re

from flask import url_for
import pytest
from webpat.db import db

from edarg.compat import HTTP, StringIO
from edarg.models import (Answer, User, Policy, Problem, Student, Test, Subset,
                          Grade)

from .fixtures import admin, answer, client, test_csv, user
from .utils import login


def test_be_admin(client, user):
    username, _ = user

    with client:
        login(client, user)

        member = db.query(User).filter_by(username=username).one()
        assert not member.is_admin

        num_admin = db.query(User).filter_by(is_admin=True).count()
        assert num_admin == 0

        res = client.get(url_for('admin.be_admin'))
        assert res.status_code == HTTP.FOUND

        member = db.query(User).filter_by(username=username).one()
        assert member.is_admin

        num_admin = db.query(User).filter_by(is_admin=True).count()
        assert num_admin == 1


def test_not_be_admin(client, admin, user):
    with client:
        username, _ = user
        login(client, user)

        res = client.get(url_for('admin.be_admin'))
        assert res.status_code == HTTP.NOT_FOUND

        member = db.query(User).filter_by(username=username).one()
        assert not member.is_admin


def test_access(client, admin):
    login(client, admin)
    res = client.get(url_for('admin.home'))
    assert res.status_code == HTTP.OK


def test_access_by_normal_user(client, user):
    login(client, user)
    res = client.get(url_for('admin.home'))
    assert res.status_code == HTTP.FOUND


def test_upload_no_data(client, admin):
    login(client, admin)
    res = client.post(url_for('admin.upload_data'))
    assert res.status_code == HTTP.BAD_REQUEST


def test_upload_data(client, admin, test_csv):
    login(client, admin)

    before_test = db.query(Test).count()
    before_problem = db.query(Problem).count()
    before_student = db.query(Student).count()
    before_answer = db.query(Answer).count()

    csv_file = StringIO(test_csv.encode('utf8'))

    res = client.post(url_for('admin.upload_data'),
                      data={'file': (csv_file, 'test.csv')})

    after_test = db.query(Test).count()
    after_problem = db.query(Problem).count()
    after_student = db.query(Student).count()
    after_answer = db.query(Answer).count()

    assert before_test + 1 == after_test
    assert before_problem + 2 == after_problem
    assert before_student + 2 == after_student
    assert before_answer + 4 == after_answer
    assert db.query(Answer).filter_by(example_score=99).count() > 0

    assert res.status_code == HTTP.OK


@pytest.mark.xfail
def test_view_grades(client, admin, answer):
    login(client, admin)
    res = client.get(url_for('admin.view_grades'))
    assert res.status_code == HTTP.OK

    trs = re.findall(r'<tr>', str(res.data))
    num_of_grades = db.query(Grade).count()
    assert len(trs) == 1 + num_of_grades  # table header + 2 grades


def test_get_policy_by_user(client, user):
    login(client, user)
    res = client.get(url_for('admin.get_policy'))
    assert res.status_code == HTTP.FOUND


def test_get_policy(client, admin):
    login(client, admin)
    res = client.get(url_for('admin.get_policy'))
    assert res.status_code == HTTP.OK
    assert 'number of references' in str(res.data)


def test_set_policy_by_user(client, user):
    login(client, user)

    res = client.post(url_for('admin.set_policy'))
    assert res.status_code == HTTP.FOUND


def test_set_policy(client, admin):
    login(client, admin)
    data = {'num_ref': '4'}
    res = client.post(url_for('admin.set_policy'), data=data)
    assert res.status_code == HTTP.OK

    assert db.query(Policy).count() > 0

    policy = db.query(Policy).one()
    assert int(policy.num_ref) == 4


def test_set_policy_with_null(client, admin):
    login(client, admin)
    data = {'num_ref': ''}
    client.post(url_for('admin.set_policy'), data=data)

    policy = db.query(Policy).one()
    assert policy.num_ref == ''


def test_set_policy_min_max_point(client, admin):
    login(client, admin)
    data = {'min_point': '3'}
    client.post(url_for('admin.set_policy'), data=data)

    policy = db.query(Policy).one()
    assert policy.min_point == 3
    assert policy.max_point == 5


def test_set_policy_min_point_with_char(client, admin):
    login(client, admin)
    data = {'min_point': 'a'}
    client.post(url_for('admin.set_policy'), data=data)

    policy = db.query(Policy).one()
    assert policy.min_point == 1
    assert policy.max_point == 5


def test_set_policy_on_timelimit(client, admin):
    login(client, admin)
    data = {'time_limit': 300}
    client.post(url_for('admin.set_policy'), data=data)

    policy = db.query(Policy).one()
    assert policy.time_limit == 300


def test_new_subset(client, admin, answer):
    login(client, admin)
    res = client.get(url_for('admin.new_subset', problem_pk=1))
    assert res.status_code == HTTP.OK


def test_edit_subset(client, admin, answer):
    login(client, admin)
    res = client.get(url_for('admin.edit_subset', subset_pk=1))
    assert res.status_code == HTTP.OK


def test_save_new_subset(client, admin):
    login(client, admin)
    before = db.query(Subset).count()
    client.post(url_for('admin.save_subset'), data={'problem': 1})
    assert db.query(Subset).count() == before + 1


def test_save_existing_subset(client, admin, answer):
    login(client, admin)
    client.post(url_for('admin.save_subset'),
                data={'pk': 1, 'answers': [1, 2, 3]})
    subset = db.query(Subset).get(1)
    assert len(subset.answers) == 3


def test_subset_add_answers(client, admin, answer):
    login(client, admin)
    client.post(url_for('admin.save_subset'),
                data={'problem': 1, 'answers': [1, 2, 3]})
    subset = db.query(Subset).all()[-1]
    assert len(subset.answers) == 3


def test_subset_change_answers(client, admin, answer):
    login(client, admin)
    with client:
        client.post(url_for('admin.save_subset'),
                    data={'pk': 2, 'problem': 2, 'answers': [2, 3, 4]})
        subset = db.query(Subset).get(2)
        assert set(x.pk for x in subset.answers) == {2, 3, 4}


def test_new_test(client, admin):
    login(client, admin)
    with client:
        res = client.get(url_for('admin.new_test'))
        assert res.status_code == HTTP.OK

        before = db.query(Test).count()
        res = client.post(url_for('admin.new_test',
                          data={'title': 'new test'}))
        assert res.status_code == HTTP.FOUND
        after = db.query(Test).count()
        assert before + 1 == after


def test_edit_test(client, admin, answer):
    login(client, admin)
    with client:
        res = client.get(url_for('admin.edit_test', test_pk=1))
        assert res.status_code == HTTP.OK
        assert re.search(r'abc', res.data.decode('utf8'))

        test = db.query(Test).get(1)
        before = len(test.problems)

        res = client.post(url_for('admin.edit_test', test_pk=1),
                          data={'question': 'new problem'})
        assert res.status_code == HTTP.OK
        assert re.search(r'new problem', res.data.decode('utf8'))

        test = db.query(Test).get(1)
        after = len(test.problems)
        assert before + 1 == after

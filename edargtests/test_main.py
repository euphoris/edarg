from flask import url_for

from .fixtures import admin, client, user
from .utils import login
from edarg.compat import HTTP

def test_home_not_login(client):
    res = client.get(url_for('main.home'))
    assert res.status_code == HTTP.FOUND
    assert url_for('member.view_login') in res.headers['location']


def test_home_as_grader(client, user):
    login(client, user)
    res = client.get(url_for('main.home'))
    assert res.status_code == HTTP.FOUND
    assert res.headers['location'].endswith(url_for('grader.home'))


def test_home_as_admin(client, admin):
    login(client, admin)
    res = client.get(url_for('main.home'))
    assert res.status_code == HTTP.FOUND
    assert res.headers['location'].endswith(url_for('admin.home'))

from flask import url_for
from flask.ext.login import current_user
from webpat.db import db

from edarg.models import GraderInfo, User
from edarg.compat import HTTP

from .fixtures import client, user
from .utils import login


def test_view_signup(client):
    res = client.get(url_for('member.view_signup'))
    assert res.status_code == HTTP.OK


def test_signup(client):
    before = db.query(User).count()

    with client:
        assert not current_user.is_active()
        res = client.post(url_for('member.signup'),
                        data={'username': 'test', 'password': '1234',
                              'gender': 1, 'birth_year': 1980})

        after = db.query(User).count()
        u, g = db.query(User, GraderInfo) \
                 .filter(User.pk == GraderInfo.grader_pk) \
                 .filter_by(username='test').one()
        assert u.username == 'test' and g.gender ==1 and g.birth_year == 1980

        assert res.status_code == HTTP.FOUND
        assert res.headers['location'].endswith(url_for('grader.home'))
        assert before + 1 == after
        assert current_user.is_active()


def test_signup_exist_user(client, user):
    username, password = user
    with client:
        res = client.post(url_for('member.signup'),
                        data={'username': username, 'password': password})
        assert res.status_code == HTTP.OK


def test_signup_not_fill_blanks(client):
    res = client.post(url_for('member.signup'))
    assert res.status_code == HTTP.OK

    res = client.post(url_for('member.signup'),
                      data={'username': '123'})
    assert res.status_code == HTTP.OK


def test_signup_already_login(client, user):
    username, password = user
    with client:
        login(client, user)
        res = client.get(url_for('member.view_signup'))
        assert res.status_code == HTTP.FOUND
        assert res.headers['location'].endswith(url_for('grader.home'))


def test_login(client, user):
    res = client.get(url_for('member.view_login'))
    assert res.status_code == HTTP.OK

    username, password = user
    with client:
        res = client.post(url_for('member.login'),
                          data={'username': username,
                                'password': password})
        assert res.status_code == HTTP.FOUND
        assert res.headers['location'].endswith(url_for('grader.home'))
        assert current_user.is_active()


def test_login_already_login(client, user):
    login(client, user)

    res = client.get(url_for('member.login'))
    assert res.status_code == HTTP.FOUND
    assert res.headers['location'].endswith(url_for('grader.home'))


def test_login_no_user(client):
    res = client.post(url_for('member.login'),
                      data={'username': u'test1',
                            'password': u'1234'})
    assert res.status_code == HTTP.OK


def test_login_password_missing(client):
    res = client.post(url_for('member.login'),
                      data={'username': u'test1'})
    assert res.status_code == HTTP.OK


def test_logout(client):
    with client:
        res = client.get(url_for('member.logout'))
        assert res.status_code == HTTP.FOUND
        assert current_user.is_anonymous()

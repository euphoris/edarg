import os
import sys

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

install_requires = ['flask>=0.9',
                    'SQLAlchemy>=0.8.1',
                    'chardet>=2.1.1',
                    'Flask-Login',
                    'WebPattern'
                    ]


if any(var.startswith('HEROKU') for var in os.environ):
    install_requires += ['gunicorn', 'psycopg2']


if sys.version_info >= (3,):
    pass
else:
    install_requires.extend(['unicodecsv>=0.9.4',
                             ])


setup(
    name='edarg',
    packages=['edarg'],
    description='a web-based grading support system',
    author='Jae-Myoung Yu',
    author_email='euphoris@gmail.com',
    maintainer='Jae-Myoung Yu',
    maintainer_email='euphoris@gmail.com',
    tests_require=['pytest'],
    install_requires=install_requires,
    entry_points={
        'console_scripts': ['edarg = edarg.scripts:run_script']
    },
)
